package com.jdc.rest.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.jdc.rest.client.CourseClient;
import com.jdc.rest.client.dto.Course;

class CourseClientTest {
	
	CourseClient client;

	@BeforeEach
	void setUp() throws Exception {
		client = new CourseClient();
	}
	
	@AfterEach
	void endsUp() {
		client.close();
	}

	@Test
	void test1() {
		List<Course> list = client.getAll();
		
		assertNotNull(list);
		assertEquals(5, list.size());
	}
	
	@Test
	void test2() {
		
		Course c1 = client.findById(1);
		
		assertNotNull(c1);
		
		assertEquals(1, c1.getId());
		assertEquals("Java SE", c1.getName());
		assertEquals(180000, c1.getPrice());
	}
	
	@Test
	void test3() {
		
		List<Course> list1 = client.search("J", 180000);
		assertNotNull(list1);
		
		assertEquals(1, list1.size());
		
		List<Course> list2 = client.search("E", 0);
		assertNotNull(list2);
		
		assertEquals(0, list2.size());
	}

}
