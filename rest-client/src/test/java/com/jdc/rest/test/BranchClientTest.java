package com.jdc.rest.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.jdc.rest.client.BranchClient;
import com.jdc.rest.client.dto.Branch;

class BranchClientTest {
	
	BranchClient client;

	@BeforeEach
	void setUp() throws Exception {
		client = new BranchClient();
	}
	
	@AfterEach
	void finish() {
		client.close();
	}

	@Test
	void test1() {
		List<Branch> list = client.getAll();
		assertNotNull(list);
		
		assertEquals(2, list.size());
	}

	@Test
	void test2() {
		Branch b = client.findById(1);
		assertNotNull(b);
		
		assertEquals("Yangon JDC", b.getName());
	}

	@Test
	void test3() {
		List<Branch> list = client.search(null, "09782003098");
		assertNotNull(list);
		
		assertEquals(2, list.size());
	}
}
