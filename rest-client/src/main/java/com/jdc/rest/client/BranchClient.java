package com.jdc.rest.client;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.jdc.rest.client.dto.Branch;

public class BranchClient implements AutoCloseable{

	private Client client;
	private static final String BASE_URL = "http://localhost:8080/rest-api/api/branches";
	
	public BranchClient() {
		client = ClientBuilder.newClient();
	}
	
	@Override
	public void close() {
		client.close();
	}
	
	public List<Branch> getAll() {
		
		// Get Target
		WebTarget target = client.target(BASE_URL);
		
		// Build Request
		Builder builder = 
				target.request(MediaType.APPLICATION_JSON);
		
		// Invoke Request
		Response response = builder.get();
		
		// Get Data from Response
		if(response.getStatusInfo() == Status.OK) {
			return response.readEntity(
					new GenericType<List<Branch>>() {});
		}
		
		return null;
	}
	
	public Branch findById(int id) {
		
		// Get Target
		WebTarget target = client.target(BASE_URL);
		
		// Adding Path to target
		target = target.path(String.valueOf(id));
		
		// Invoke Request
		Response response = target.request().get();
		
		// Get Data from Response
		if(response.getStatusInfo() == Status.OK) {
			return response.readEntity(Branch.class);
		}

		return null;
	}

	public List<Branch> search(String name, String phone) {
		
		// Get Target
		WebTarget target = client.target(BASE_URL)
				.queryParam("n", name)
				.queryParam("p", phone);
		
		// Build Request
		Builder builder = 
				target.request(MediaType.APPLICATION_JSON);
		
		// Invoke Request
		Response response = builder.get();
		
		// Get Data from Response
		if(response.getStatusInfo() == Status.OK) {
			return response.readEntity(
					new GenericType<List<Branch>>() {});
		}
		
		return null;
	}
}
