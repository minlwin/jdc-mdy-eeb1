package com.jdc.rest.client;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.jdc.rest.client.dto.Course;

public class CourseClient implements AutoCloseable{
	
	private static final String BASE_URL = "http://localhost:8080/rest-api/api/courses";
	
	private Client client;
	
	
	public CourseClient() {
		// create client
		client = ClientBuilder.newClient();
	}

	public List<Course> getAll() {
		
		// set target
		WebTarget target = client.target(BASE_URL);
		
		// create request
		Builder builder = target.request(MediaType.APPLICATION_JSON);
		
		// Invoke Request
		Response response = builder.get();
		
		// get data from response
		if(response.getStatusInfo() == Status.OK) {
			List<Course> list = response.readEntity(new GenericType<List<Course>>() {});
			
			return list;
		}
		
		return null;
	}
	
	/**
	 * Add Path to request
	 * 
	 * @param id
	 * @return
	 */
	public Course findById(int id) {
		
		Response response = client.target(BASE_URL)
			.path(String.valueOf(id))
			.request(MediaType.APPLICATION_JSON)
			.get();
		
		if(response.getStatusInfo() == Status.OK) {
			return response.readEntity(Course.class);
		}
		
		return null;
	}
	
	public List<Course> search(String name, int price) {
		
		Response response = ClientBuilder.newClient().target(BASE_URL)
				.path("search")
				.queryParam("n", name)
				.queryParam("p", price)
				.request(MediaType.APPLICATION_JSON)
				.get();
		
		if(response.getStatusInfo() == Status.OK) {
			return response.readEntity(new GenericType<List<Course>>() {});
		}
		
		return null;
	}

	@Override
	public void close() {
		client.close();
	}

}
