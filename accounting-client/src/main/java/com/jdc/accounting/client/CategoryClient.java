package com.jdc.accounting.client;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.jdc.accounting.entity.Category;

public class CategoryClient {

	public List<Category> getAllCategory() {
		
		// get client
		Client client = ClientBuilder.newClient();
		
		// set target
		WebTarget target = client.target("http://localhost:8080/accounting/rest/categories");
		
		// create request
		// submit request
		Response response = target.request(MediaType.APPLICATION_JSON).get();
		
		if(response.getStatus() == 200) {
			// read body message
			List<Category> list = response.readEntity(new GenericType<List<Category>>() {});
			
			return list;
		}
		
		return null;
	}
	
	public Category findById(int id) {
		
		// get client
		Client client = ClientBuilder.newClient();
		
		// set target
		WebTarget target = client.target("http://localhost:8080/accounting/rest/categories/");
		
		target = target.path(String.valueOf(id));
		// create request
		// submit request
		Response response = target.request(MediaType.APPLICATION_JSON).get();
		
		if(response.getStatus() == 200) {
			return response.readEntity(Category.class);
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		
		CategoryClient client = new CategoryClient();
		List<Category> list = client.getAllCategory();
		
		for(Category c : list) {
			System.out.println(c.getName());
		}
		
		Category cat = client.findById(1);
		System.out.println(cat.getName());
	}
}
