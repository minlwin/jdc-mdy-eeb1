insert into Course (name, price) values ('Java SE', 180000);
insert into Course (name, price) values ('Java EE', 300000);
insert into Course (name, price) values ('Spring', 300000);
insert into Course (name, price) values ('Kotlin Basic', 180000);
insert into Course (name, price) values ('Kotlin Android Basic', 200000);

insert into Branch (name, phone, address) values ('Yangon JDC', '09782003098', 'Kamayut, Yangon');
insert into Branch (name, phone, address) values ('Mandalay JDC', '09782003098', '38, 73 - 73 Street, Mandalay');
