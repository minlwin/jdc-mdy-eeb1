package com.jdc.rest.resources;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.jdc.rest.entity.Branch;

@LocalBean
@Stateless
@Path("branches")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BranchResources {

	@PersistenceContext
	private EntityManager em;
	
	@GET
	public List<Branch> getAll() {
		return em.createNamedQuery("Branch.getAll", Branch.class).getResultList();
	}
	
	@GET
	@Path("{id:\\d+}")
	public Response findById(@PathParam("id") int id) {
		
		Branch c = em.find(Branch.class, id);
		
		if(null != c) {
			return Response.ok(c).build();
		}
		
		return Response.noContent().build();
	}
	
	@GET
	@Path("search")
	public List<Branch> search(@QueryParam("n") String name, 
			@QueryParam("p") String phone) {
		
		StringBuffer sb = new StringBuffer("select b from Branch b where 1 = 1");
		Map<String, Object> params = new HashMap<>();
		
		if(null != name) {
			sb.append(" and upper(c.name) like upper(:name)");
			params.put("name", name.concat("%"));
		}
		
		if(phone != null) {
			sb.append(" and c.price like :price");
			params.put("phone", phone.concat("%"));
		}
		
		TypedQuery<Branch> query = em.createQuery(sb.toString(), Branch.class);
		
		for(String key : params.keySet()) {
			query.setParameter(key, params.get(key));
		}
		
		return query.getResultList();
	}

}
