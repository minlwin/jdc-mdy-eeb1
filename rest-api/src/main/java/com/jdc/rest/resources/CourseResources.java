package com.jdc.rest.resources;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.jdc.rest.entity.Course;

@LocalBean
@Stateless
@Path("courses")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CourseResources {

	@PersistenceContext
	private EntityManager em;
	
	@GET
	public List<Course> getAll() {
		return em.createNamedQuery("Course.getAll", Course.class).getResultList();
	}
	
	@GET
	@Path("{id:\\d+}")
	public Response findById(@PathParam("id") int id) {
		
		Course c = em.find(Course.class, id);
		
		if(null != c) {
			return Response.ok(c).build();
		}
		
		return Response.noContent().build();
	}
	
	@GET
	@Path("search")
	public List<Course> search(@QueryParam("n") String name, 
			@QueryParam("p") @DefaultValue("0") int price) {
		
		StringBuffer sb = new StringBuffer("select c from Course c where 1 = 1");
		Map<String, Object> params = new HashMap<>();
		
		if(null != name) {
			sb.append(" and upper(c.name) like upper(:name)");
			params.put("name", name.concat("%"));
		}
		
		if(price > 0) {
			sb.append(" and c.price <= :price");
			params.put("price", price);
		}
		
		TypedQuery<Course> query = em.createQuery(sb.toString(), Course.class);
		
		for(String key : params.keySet()) {
			query.setParameter(key, params.get(key));
		}
		
		return query.getResultList();
	}
}
