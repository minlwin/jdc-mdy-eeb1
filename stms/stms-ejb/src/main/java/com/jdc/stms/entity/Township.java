package com.jdc.stms.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
@Entity
public class Township implements Serializable {
	private static final long serialVersionUID = 1L;

	public Township() {
    }
    @Id
    @GeneratedValue(strategy = IDENTITY)
	private int id;

    private String name;

    @ManyToOne
	private Division division;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Division getDivision() {
		return division;
	}

	public void setDivision(Division division) {
		this.division = division;
	}
    
}