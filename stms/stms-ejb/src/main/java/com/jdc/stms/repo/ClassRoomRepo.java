package com.jdc.stms.repo;

import com.jdc.stms.entity.ClassRoom;

public class ClassRoomRepo extends AbstractRepository<ClassRoom> {

	@Override
	protected Class<ClassRoom> getType() {
		return ClassRoom.class;
	}

}