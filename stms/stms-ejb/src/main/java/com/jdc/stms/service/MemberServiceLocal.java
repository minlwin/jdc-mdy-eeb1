package com.jdc.stms.service;

import javax.ejb.Local;

import com.jdc.stms.entity.Member;

@Local
public interface MemberServiceLocal {

	Member findById(String loginId);

}
