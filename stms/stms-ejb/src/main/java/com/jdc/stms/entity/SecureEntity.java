package com.jdc.stms.entity;

import java.io.Serializable;

public interface SecureEntity extends Serializable{

	void setSecurity(SecurityInfo security);
	
	SecurityInfo getSecurity();
}
