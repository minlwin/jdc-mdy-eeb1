package com.jdc.stms.repo;

import com.jdc.stms.entity.Division;

public class DivisionRepo extends AbstractRepository<Division> {

	@Override
	protected Class<Division> getType() {
		return Division.class;
	}


}