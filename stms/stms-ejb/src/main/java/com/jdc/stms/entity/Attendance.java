package com.jdc.stms.entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Attendance implements SecureEntity{

    
	private static final long serialVersionUID = 1L;

	public Attendance() {
    }


    @EmbeddedId
    private AttendancePK id;

    @ManyToOne
	@JoinColumn(name = "applicationId", insertable=false, updatable=false)
	private StuentApplication student;

    private Status status;
    
    private SecurityInfo security;

    public enum Status {
        Attend,
        Late,
        Absent,
        Leave
    }

	public StuentApplication getStudent() {
		return student;
	}

	public void setStudent(StuentApplication student) {
		this.student = student;
	}

	public AttendancePK getId() {
		return id;
	}

	public void setId(AttendancePK id) {
		this.id = id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

}