package com.jdc.stms.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class YearlyRoom implements SecureEntity {

	private static final long serialVersionUID = 1L;

	public YearlyRoom() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;

	@ManyToOne
	private YearlyGrade grade;

	@ManyToOne
	private ClassRoom room;

	private TimeType timeType;

	private SecurityInfo security;

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public enum TimeType {
		Morning, Evening
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public YearlyGrade getGrade() {
		return grade;
	}

	public void setGrade(YearlyGrade grade) {
		this.grade = grade;
	}

	public ClassRoom getRoom() {
		return room;
	}

	public void setRoom(ClassRoom room) {
		this.room = room;
	}

	public TimeType getTimeType() {
		return timeType;
	}

	public void setTimeType(TimeType timeType) {
		this.timeType = timeType;
	}

}