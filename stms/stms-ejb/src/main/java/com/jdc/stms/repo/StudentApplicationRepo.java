package com.jdc.stms.repo;

public class StudentApplicationRepo extends AbstractRepository<StudentApplicationRepo> {

	@Override
	protected Class<StudentApplicationRepo> getType() {

		return StudentApplicationRepo.class;
	}

}