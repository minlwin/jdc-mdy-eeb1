package com.jdc.stms.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ClassRoom implements SecureEntity {

	private static final long serialVersionUID = -5745588046645873444L;

	public ClassRoom() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;

	private String roomNumber;

	private String buliding;

	private String floor;

	private SecurityInfo security;

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}

	public String getBuliding() {
		return buliding;
	}

	public void setBuliding(String buliding) {
		this.buliding = buliding;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

}