package com.jdc.stms.common;

import java.time.LocalDateTime;

import javax.ejb.EJBContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import com.jdc.stms.entity.SecureEntity;
import com.jdc.stms.entity.SecurityInfo;

public class SecurityListener {

	@PrePersist
	public void prePersist(Object entity) {
		if (entity instanceof SecureEntity) {
			SecureEntity sec = (SecureEntity) entity;
			SecurityInfo s = new SecurityInfo();
			s.setCreateTime(LocalDateTime.now());
			s.setUpdateTime(LocalDateTime.now());
			s.setCreateUser(getLoginUser());
			s.setUpdateUser(getLoginUser());
			sec.setSecurity(s);
		}
	}

	@PreUpdate
	public void preUpdate(Object entity) {
		if (entity instanceof SecureEntity) {
			SecureEntity sec = (SecureEntity) entity;
			SecurityInfo s = sec.getSecurity();
			s.setUpdateTime(LocalDateTime.now());
			s.setUpdateUser(getLoginUser());
		}
	}

	private String getLoginUser() {
		try {
			EJBContext ctx = (EJBContext) new InitialContext().lookup("java:comp/EJBContext");
			return ctx.getCallerPrincipal().getName();
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
