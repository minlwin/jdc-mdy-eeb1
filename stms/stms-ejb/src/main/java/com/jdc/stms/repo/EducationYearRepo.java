package com.jdc.stms.repo;

import com.jdc.stms.entity.EducationYear;

public class EducationYearRepo extends AbstractRepository<EducationYear> {

	@Override
	protected Class<EducationYear> getType() {
		return EducationYear.class;
	}

 
}