package com.jdc.stms.common;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class PasswordUtils {
	
	public static String encprit(String raw) {
		
		try {
			byte [] data = raw.getBytes();
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte [] digestData = digest.digest(data);
			return Base64.getEncoder().encodeToString(digestData);
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		System.out.println(encprit("admin"));
	}

}
