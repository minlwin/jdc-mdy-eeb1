package com.jdc.stms.repo;

public class YearGradeRepo extends AbstractRepository<YearGradeRepo> {

	@Override
	protected Class<YearGradeRepo> getType() {

		return YearGradeRepo.class;
	}

}