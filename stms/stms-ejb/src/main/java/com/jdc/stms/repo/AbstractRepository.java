package com.jdc.stms.repo;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

public abstract class AbstractRepository<T> {

	@PersistenceContext
	private EntityManager em;
	
	protected abstract Class<T> getType();

	public void create(T t) {
		em.persist(t);
	}

	public void update(T t) {
		em.merge(t);
	}

	public void delete(Object id) {
		em.remove(findById(id));
	}

	public T findById(Object id) {
		return em.find(getType(), id);
	}
	
	public List<T> findByNamedQuery(String name, Map<String, Object> params) {
		
		TypedQuery<T> query = em.createNamedQuery(name, getType());
		
		if(null != params) {
			for(String key : params.keySet()) {
				query.setParameter(key, params.get(key));
			}
		}
		
		return query.getResultList();
	}
	
	public List<T> findByQuery(String sql, Map<String, Object> params) {
		
		TypedQuery<T> query = em.createQuery(sql, getType());
		
		if(null != params) {
			for(String key : params.keySet()) {
				query.setParameter(key, params.get(key));
			}
		}
		
		return query.getResultList();
	}

}