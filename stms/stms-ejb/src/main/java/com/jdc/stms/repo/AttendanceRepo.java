package com.jdc.stms.repo;

import com.jdc.stms.entity.Attendance;

public class AttendanceRepo extends AbstractRepository<Attendance> {

	@Override
	protected Class<Attendance> getType() {
		return Attendance.class;
	}

}