package com.jdc.stms.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class YearlyGrade implements SecureEntity {

	private static final long serialVersionUID = 1L;

	public YearlyGrade() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;

	@ManyToOne
	private EducationYear eduYear;

	@ManyToOne
	private Grade grade;

	private SecurityInfo security;

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EducationYear getEduYear() {
		return eduYear;
	}

	public void setEduYear(EducationYear eduYear) {
		this.eduYear = eduYear;
	}

	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

}