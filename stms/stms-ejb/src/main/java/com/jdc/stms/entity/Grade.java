package com.jdc.stms.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Grade implements SecureEntity {

	private static final long serialVersionUID = 1L;

	public Grade() {
	}

	@Id
	private int id;

	private String name;

	private String description;

	private int viewOrder;

	private SecurityInfo security;

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getViewOrder() {
		return viewOrder;
	}

	public void setViewOrder(int viewOrder) {
		this.viewOrder = viewOrder;
	}

}