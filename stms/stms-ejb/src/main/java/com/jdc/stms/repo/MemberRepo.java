package com.jdc.stms.repo;

import com.jdc.stms.entity.Member;

public class MemberRepo extends AbstractRepository<Member> {

	@Override
	protected Class<Member> getType() {
		return Member.class;
	}

}
