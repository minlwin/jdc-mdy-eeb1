package com.jdc.stms.entity;

import javax.persistence.Entity;

@Entity
public class Student extends Member {

	private static final long serialVersionUID = 1L;

	public Student() {
		address = new Address();
		setRole(Role.Student);
	}

	private String nrc;

	private String phone;

	private String emial;

	private Address address;
	
	public String getNrc() {
		return nrc;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmial() {
		return emial;
	}

	public void setEmial(String emial) {
		this.emial = emial;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

}