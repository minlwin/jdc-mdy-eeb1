package com.jdc.stms.repo;

public class GradeRepo extends AbstractRepository<GradeRepo> {

	@Override
	protected Class<GradeRepo> getType() {

		return GradeRepo.class;
	}

}