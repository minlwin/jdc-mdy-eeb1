package com.jdc.stms.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class Division implements Serializable{

    
	private static final long serialVersionUID = 2182115650156543433L;

	public Division() {
    }

	@Id
    @GeneratedValue(strategy = IDENTITY)
	private int id;

    private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    

}