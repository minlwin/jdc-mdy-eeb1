package com.jdc.stms.service.impl;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.jdc.stms.entity.Member;
import com.jdc.stms.repo.MemberRepo;
import com.jdc.stms.service.MemberServiceLocal;

@Stateless
public class MemberService implements MemberServiceLocal {
	
	@Inject
	private MemberRepo repo;
	
	@Override
	public Member findById(String loginId) {
		return repo.findById(loginId);
	}

}
