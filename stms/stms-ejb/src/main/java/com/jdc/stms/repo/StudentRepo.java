package com.jdc.stms.repo;

public class StudentRepo extends AbstractRepository<StudentRepo> {

	@Override
	protected Class<StudentRepo> getType() {

		return StudentRepo.class;
	}

}