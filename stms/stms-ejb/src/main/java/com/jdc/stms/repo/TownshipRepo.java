package com.jdc.stms.repo;

public class TownshipRepo extends AbstractRepository<TownshipRepo> {

	@Override
	protected Class<TownshipRepo> getType() {

		return TownshipRepo.class;
	}

}