package com.jdc.stms.web.controller;

import javax.enterprise.event.Event;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.jdc.stms.web.common.LoginInfo;

@Model
public class LoginBean {

	private String loginId;
	private String password;
	
	@Inject
	private Event<LoginInfo> event;

	public String login() {
		
		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			request.login(loginId, password);
			
			event.fire(new LoginInfo(loginId));
			
			if(request.isUserInRole("Admin")) {
				return "/views/admin/home?faces-redirect=true";
			} else if(request.isUserInRole("Student")) {
				return "/views/student/home?faces-redirect=true";
			}
			
		} catch (ServletException e) {
			FacesMessage message = new FacesMessage(null, "Please Check your Login information.");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		return "";
	}

	public String logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/views/home?faces-redirect=true";
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
