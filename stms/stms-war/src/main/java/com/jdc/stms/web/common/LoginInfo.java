package com.jdc.stms.web.common;

public class LoginInfo {

	public LoginInfo(String loginId) {
		super();
		this.loginId = loginId;
	}

	private String loginId;

	public LoginInfo() {
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

}
