package com.jdc.stms.web.common;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.stms.entity.Member;
import com.jdc.stms.service.MemberServiceLocal;

@SessionScoped
public class LoginUserProducer implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Named
	@Produces
	private Member loginUser;
	
	@Inject
	private MemberServiceLocal service;
	
	public void load(@Observes LoginInfo login) {
		loginUser = service.findById(login.getLoginId());
	}
}
