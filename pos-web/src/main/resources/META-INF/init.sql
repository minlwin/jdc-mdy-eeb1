insert into Category (name) values ('Drinks');
insert into Category (name) values ('Foods');
insert into Category (name) values ('Fashion');
insert into Category (name) values ('Phones & Tablets');
insert into Category (name) values ('Phone Accessories');
insert into Category (name) values ('Computer');
insert into Category (name) values ('Computer Parts');
insert into Category (name) values ('Computer Accessories');

insert into Product (category_id, name, delFlg) values (1, 'Coke', false);
insert into Product (category_id, name, delFlg) values (1, 'Pepsi', false);
insert into Product (category_id, name, delFlg) values (1, 'Red Bull', false);
insert into Product (category_id, name, delFlg) values (1, 'Blue Mountain', false);
insert into Product (category_id, name, delFlg) values (1, 'Fire', false);

insert into Product (category_id, name, delFlg) values (2, 'Pucci Cake', false);
insert into Product (category_id, name, delFlg) values (2, 'Humberger', false);
insert into Product (category_id, name, delFlg) values (2, 'Sousage', false);
insert into Product (category_id, name, delFlg) values (2, 'Potato Chips', false);
insert into Product (category_id, name, delFlg) values (2, 'Fish Chips', false);

insert into Product (category_id, name, delFlg) values (3, 'Mens T Shirt', false);
insert into Product (category_id, name, delFlg) values (3, 'Sun Glass', false);
insert into Product (category_id, name, delFlg) values (3, 'Hat', false);
insert into Product (category_id, name, delFlg) values (3, 'Hancachif', false);

