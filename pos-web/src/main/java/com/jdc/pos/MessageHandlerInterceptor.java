package com.jdc.pos;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@MessageHandler
@Interceptor
public class MessageHandlerInterceptor implements Serializable {

	private static final long serialVersionUID = 1L;

	@AroundInvoke
	public Object aroundInvoke(InvocationContext ic) throws Exception {
		
		Object result = null;
		
		try {
			result = ic.proceed();
			
		} catch (PosValidationException e) {
			// create jsf message
			FacesMessage message = new FacesMessage("Error", e.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		
		return result;
	}

}
