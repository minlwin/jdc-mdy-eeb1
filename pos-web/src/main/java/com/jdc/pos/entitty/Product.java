package com.jdc.pos.entitty;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
@NamedQueries({
	@NamedQuery(name = "Product.findByCategory", query = "select p from Product p where p.category = :category"),
	@NamedQuery(name = "Product.findCountByCategory", query = "select count(p) from Product p where p.category = :category"),
	@NamedQuery(name = "Product.findByDateAndPrice", query = "select p from Product p join p.prices pr "
			+ "where pr.price <= :price and pr.id.productId = (select max(sp.id.productId) from SellPrice sp where sp.id.refDate <= :date) "
			+ "and pr.id.refDate = (select max(sp.id.refDate) from SellPrice sp where sp.id.refDate <= :date)"),
	@NamedQuery(name = "Product.findByCategoryAndName", query = "select p from Product p where p.name = :name and p.category = :category and p.security.delFlg = false")
})
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;

	public Product() {
		security = new SecurityInfo();
		prices = new ArrayList<>();
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private Integer id;
	
	private String name;

	private String image;

	private SecurityInfo security;

	@NotNull(message = "Please select one Category!")
	@ManyToOne
	private Category category;
	
	public void addPrice(SellPrice price) {
		price.setProduct(this);
		prices.add(price);
	}
	
	@OneToMany(mappedBy = "product", cascade = { PERSIST, MERGE }, orphanRemoval = true, fetch = EAGER)
	private List<SellPrice> prices;
	
	public SellPrice getCuttentPrice() {
		return prices.stream()
				.sorted((a , b) -> b.getId().getRefDate().compareTo(a.getId().getRefDate()))
				.filter(p -> p.getId().getRefDate().compareTo(LocalDate.now()) <= 0)
				.findFirst().orElse(null);			
	}
	

	public List<SellPrice> getPrices() {
		return prices;
	}

	public void setPrices(List<SellPrice> prices) {
		this.prices = prices;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


	public List<SellPrice> getAttivePrices() {
		return prices.stream()
				.filter(a -> !a.getSecurity().isDelFlg())
				.sorted((a, b) -> b.getId().getRefDate().compareTo(a.getId().getRefDate()))
				.collect(Collectors.toList());
	}
	
	

}