package com.jdc.pos.entitty;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = "PurchaseItem.findByProduct", 
			query = "select p from PurchaseItem p where p.product.id = :productId and p.security.delFlg = false order by p.refDate desc")
})
public class PurchaseItem extends StockAction implements Serializable {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	private Invoice invoice;
	
	public PurchaseItem() {
		setAction(Action.Purchase);
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
	
	
}