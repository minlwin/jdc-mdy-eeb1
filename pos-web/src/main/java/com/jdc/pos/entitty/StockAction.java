package com.jdc.pos.entitty;

import static javax.persistence.GenerationType.IDENTITY;
import static javax.persistence.InheritanceType.JOINED;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.ManyToOne;

@Entity
@Inheritance(strategy = JOINED)
public abstract class StockAction implements Serializable {

	private static final long serialVersionUID = 1L;

	public StockAction() {
		security = new SecurityInfo();
		product = new Product();
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long id;

	private LocalDate refDate;

	private Integer price;

	private Integer quentity;

	private SecurityInfo security;

	@ManyToOne
	private Product product;

	private Action action;
	
	public Integer getTotal() {
		
		if(null != price && null != quentity) {
			return price * quentity;
		}
		
		return null;
	}
	

	public enum Action {
		Sale,
		Purchase
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDate getRefDate() {
		return refDate;
	}

	public void setRefDate(LocalDate refDate) {
		this.refDate = refDate;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getQuentity() {
		return quentity;
	}

	public void setQuentity(Integer quentity) {
		this.quentity = quentity;
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

}