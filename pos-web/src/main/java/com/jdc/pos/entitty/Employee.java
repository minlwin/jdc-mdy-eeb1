package com.jdc.pos.entitty;

import static javax.persistence.EnumType.STRING;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Employee implements Serializable {

	private static final long serialVersionUID = 1L;

	public Employee() {
		security = new SecurityInfo();
		contact = new ContactInfo();
	}

	@Id
	@NotEmpty(message="Please enter Employee Code!")
	private String loginId;

	@NotEmpty(message="Please enter Employee Name!")
	private String name;
	
	private String password;

	private SecurityInfo security;

	@Enumerated(STRING)
	private Role role;

	private ContactInfo contact;
	

	public enum Role {
		Manager,
		Sale
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public ContactInfo getContact() {
		return contact;
	}

	public void setContact(ContactInfo contact) {
		this.contact = contact;
	}

}