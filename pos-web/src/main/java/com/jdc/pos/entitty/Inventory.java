package com.jdc.pos.entitty;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Inventory implements Serializable {

	private static final long serialVersionUID = 1L;

	public Inventory() {
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long id;

	private int stock;

	private SecurityInfo security;

	@ManyToOne
	private Product product;

	@OneToOne
	private StockAction action;

	@OneToOne
	private Inventory lastStock;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public StockAction getAction() {
		return action;
	}

	public void setAction(StockAction action) {
		this.action = action;
	}

	public Inventory getLastStock() {
		return lastStock;
	}

	public void setLastStock(Inventory lastStock) {
		this.lastStock = lastStock;
	}
	
	

}