package com.jdc.pos.entitty;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.MERGE;

@Entity
public class Invoice implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long id;
	private LocalDate refDate;
	private Integer subTotal;
	private Integer transport;
	private Integer tax;
	private Integer total;
	@ManyToOne
	private PurchaseShop shop;
	private SecurityInfo security;

	@OneToMany(mappedBy = "invoice", cascade = { PERSIST, MERGE }, orphanRemoval = true)
	private List<PurchaseItem> items;
	
	public Invoice() {
		items = new ArrayList<>();
		security = new SecurityInfo();
		refDate = LocalDate.now();
	}
	
	public void addItem(PurchaseItem item) {
		item.setInvoice(this);
		items.add(item);
		
		calculate();
	}

	private void calculate() {
		subTotal = items.stream().mapToInt(i -> i.getTotal()).sum();
		Double tax = subTotal * 0.05;
		this.tax = tax.intValue();
		total = subTotal + this.tax + ((null != transport) ? transport : 0);
	}

	public Integer getTransport() {
		return transport;
	}

	public void setTransport(Integer transport) {
		this.transport = transport;
	}

	public List<PurchaseItem> getItems() {
		return items;
	}

	public void setItems(List<PurchaseItem> items) {
		this.items = items;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDate getRefDate() {
		return refDate;
	}

	public void setRefDate(LocalDate refDate) {
		this.refDate = refDate;
	}

	public Integer getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Integer subTotal) {
		this.subTotal = subTotal;
	}

	public Integer getTax() {
		return tax;
	}

	public void setTax(Integer tax) {
		this.tax = tax;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public PurchaseShop getShop() {
		return shop;
	}

	public void setShop(PurchaseShop shop) {
		this.shop = shop;
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public void removeItem(PurchaseItem item) {
		item.setInvoice(null);
		items.remove(item);
		calculate();
	}

}
