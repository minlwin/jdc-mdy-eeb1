package com.jdc.pos.entitty;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = "PaidHistory.findByInvoice", query = "select p from PaidHistory p where p.id.invoiceId = :invoice_id")
})
public class PaidHistory implements Serializable {

	private static final long serialVersionUID = 1L;

	public PaidHistory() {
		id = new PaidHistoryPK();
		security = new SecurityInfo();
	}

	private int paid;

	private int lastRemain;

	private SecurityInfo security;


	@EmbeddedId
	private PaidHistoryPK id;
	
	@ManyToOne
	@JoinColumn(name = "invoiceId", insertable = false, updatable = false)
	private Invoice invoice;
	
	private String remark;
	

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public int getPaid() {
		return paid;
	}

	public void setPaid(int paid) {
		this.paid = paid;
	}

	public int getLastRemain() {
		return lastRemain;
	}

	public void setLastRemain(int lastRemain) {
		this.lastRemain = lastRemain;
	}

	public SecurityInfo getSecurity() {
		return security;
	}

	public void setSecurity(SecurityInfo security) {
		this.security = security;
	}

	public PaidHistoryPK getId() {
		return id;
	}

	public void setId(PaidHistoryPK id) {
		this.id = id;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
		this.id.setInvoiceId(invoice.getId());
	}



}