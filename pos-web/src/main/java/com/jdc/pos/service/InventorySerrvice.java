package com.jdc.pos.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.jdc.pos.entitty.Category;
import com.jdc.pos.entitty.PurchaseItem;
import com.jdc.pos.entitty.SaleItem;
import com.jdc.pos.entitty.StockAction;
import com.jdc.pos.repo.PurchaseItemRepo;
import com.jdc.pos.repo.SaleItemRepo;

@LocalBean
@Stateless
public class InventorySerrvice {

	@Inject
	private SaleItemRepo sale;
	@Inject
	private PurchaseItemRepo purchase;
	
	public List<StockAction> search(Category category, String productName, LocalDate dateFrom, LocalDate dateTo) {
		
		StringBuffer sb = new StringBuffer();
		Map<String, Object> params = new HashMap<>();
		
		if(null != category) {
			sb.append("and t.product.category = :category ");
			params.put("category", category);
		}
		
		if(null != productName && !productName.isEmpty()) {
			sb.append("and upper(t.product.name) like upper(:name) ");
			params.put("name", productName.concat("%"));
		}
		
		if(null != dateFrom) {
			sb.append("and t.refDate >= :from ");
			params.put("from", dateFrom);
		}
		
		if(null != dateTo) {
			sb.append("and t.refDate <= :to ");
			params.put("to", dateTo);
		}
		
		String purchaseSql = "select t from PurchaseItem t where 1 = 1 ".concat(sb.toString());
		
		List<PurchaseItem> purchaseList = purchase.select(purchaseSql, params);
		
		String saleSql = "select t from SaleItem t where 1 = 1 ".concat(sb.toString());
		List<SaleItem> saleList = sale.select(saleSql, params);
		
		List<StockAction> result = new ArrayList<>();
		result.addAll(purchaseList);
		result.addAll(saleList);
		
		Collections.sort(result, (a,b) -> a.getRefDate().compareTo(b.getRefDate()));
		
		return result;
	}
}
