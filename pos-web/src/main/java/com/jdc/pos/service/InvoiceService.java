package com.jdc.pos.service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.jdc.pos.entitty.Invoice;
import com.jdc.pos.entitty.PaidHistory;
import com.jdc.pos.entitty.Product;
import com.jdc.pos.entitty.PurchaseItem;
import com.jdc.pos.repo.InvoiceRepo;
import com.jdc.pos.repo.PaidHistoryRepo;

@LocalBean
@Stateless
public class InvoiceService {

	@Inject
	private InvoiceRepo repo;
	
	@Inject
	private PurchaseShopService shopService;
	@Inject
	private ProductService productService;
	@Inject
	private PaidHistoryRepo paidRepo;
	

	public List<Invoice> search(String shop, LocalDate dateFrom, LocalDate dateTo) {
		
		StringBuffer sb = new StringBuffer("select i from Invoice i where i.security.delFlg = false ");
		Map<String, Object> params = new HashMap<>();
		
		if(null != shop && !shop.isEmpty()) {
			sb.append("and upper(i.shop.name) like upper(:name) ");
			params.put("name", shop.concat("%"));
		}
		
		if(null != dateFrom) {
			sb.append("and i.refDate >= :from ");
			params.put("from", dateFrom);
		}
		
		if(null != dateTo) {
			sb.append("and i.refDate <= :to ");
			params.put("to", dateTo);
		}
		
		return repo.select(sb.toString(), params);
	}

	public void save(Invoice invoice, List<PaidHistory> paidList) {

		// shop 
		if(null == invoice.getShop().getId() || invoice.getShop().getId() == 0) {
			shopService.save(invoice.getShop());
		}
		
		// products
		for(PurchaseItem item : invoice.getItems()) {
			Product p = productService.findByCategoryAndName(item.getProduct().getCategory(), item.getProduct().getName());
			
			if(null == p) {
				p = productService.save(item.getProduct());
			}
			
			item.setProduct(p);
			item.setRefDate(invoice.getRefDate());
		}
		
		// invoice
		invoice = repo.update(invoice);
		
		// paid history
		for(PaidHistory p : paidList) {
			p.setInvoice(invoice);
			paidRepo.update(p);
		}
		
	}

	public Invoice findById(long id) {
		
		Invoice invoice = repo.find(id);
		invoice.getItems().size();
		
		return invoice;
	}

	public List<PaidHistory> findByInvoiceId(long id) {
		Map<String, Object> params = new HashMap<>();
		params.put("invoice_id", id);
		return paidRepo.selectNamedQuery("PaidHistory.findByInvoice", params);
	}
	
	
}
