package com.jdc.pos.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.jdc.pos.entitty.Employee;
import com.jdc.pos.entitty.Employee.Role;
import com.jdc.pos.repo.EmployeeRepo;

@LocalBean
@Stateless
public class EmployeeService {
	
	@Inject
	private EmployeeRepo repo;

	public void save(Employee employee) {
		repo.update(employee);
	}

	public List<Employee> search(Role role, String name) {
		
		StringBuffer sb = new StringBuffer("select e from Employee e where 1 = 1");
		Map<String, Object> params = new HashMap<>();
		
		if(null != role) {
			sb.append(" and e.role = :role");
			params.put("role", role);
		}
		
		if(null != name && !name.isEmpty()) {
			sb.append(" and e.name like :name");
			params.put("name", name.concat("%"));
		}
		
		return repo.select(sb.toString(), params);
	}

}
