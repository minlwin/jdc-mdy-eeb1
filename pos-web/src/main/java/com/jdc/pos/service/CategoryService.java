package com.jdc.pos.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;

import com.jdc.pos.entitty.Category;
import com.jdc.pos.repo.CategoryRepo;

@LocalBean
@Stateless
public class CategoryService {

	@Inject
	private CategoryRepo repo;
	@Inject
	private ProductService productService;

	@Inject
	private Event<Category> event;
	
	public void save(Category category) {
		repo.update(category);
		event.fire(category);
	}

	public List<Category> search(String name) {
		StringBuffer sb = new StringBuffer("select c from Category c where 1 = 1");
		Map<String, Object> params = new HashMap<>();
		
		if(null != name && !name.isEmpty()) {
			sb.append(" and lower(c.name) like lower(:name)");
			params.put("name", name.concat("%"));
		}
		
		return repo.select(sb.toString(), params);
	}

	public Category findById(int id) {
		return repo.find(id);
	}

	public List<Category> findForIndexPage() {
		
		List<Category> categories = repo.selectNamedQuery("Category.getAll", null);
		
		for(Category c : categories) {
			c.setProductCount(productService.findCountByCategory(c));
		}
		
		return categories;
	}
}
