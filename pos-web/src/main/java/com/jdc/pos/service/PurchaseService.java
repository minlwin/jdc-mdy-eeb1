package com.jdc.pos.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.jdc.pos.entitty.Product;
import com.jdc.pos.entitty.PurchaseItem;
import com.jdc.pos.repo.PurchaseItemRepo;

@LocalBean
@Stateless
public class PurchaseService {
	
	@Inject
	private PurchaseItemRepo repo;

	public List<PurchaseItem> findByProduct(Product product) {
		
		Map<String, Object> params = new HashMap<>();
		params.put("productId", product.getId());
		
		return repo.selectNamedQuery("PurchaseItem.findByProduct", params);
	}

}
