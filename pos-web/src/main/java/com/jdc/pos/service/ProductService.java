package com.jdc.pos.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.jdc.pos.PosValidationException;
import com.jdc.pos.entitty.Category;
import com.jdc.pos.entitty.Product;
import com.jdc.pos.entitty.PurchaseItem;
import com.jdc.pos.repo.ProductRepo;

@LocalBean
@Stateless
public class ProductService {
	
	@Inject
	private ProductRepo repo;
	
	@Inject
	private PurchaseService purchaseService;

	public Product findById(int id) {
		return repo.find(id);
	}

	public long findCountByCategory(Category c) {
		
		Map<String, Object> params = new HashMap<>();
		params.put("category", c);
		
		return repo.selectCountByNamedQuery("Product.findCountByCategory", params);
	}

	public List<Product> findByCategory(Category c) {
		Map<String, Object> params = new HashMap<>();
		params.put("category", c);
		
		return repo.selectNamedQuery("Product.findByCategory", params);
	}

	public Product save(Product product) {
		
		if(null == product.getName() || product.getName().isEmpty()) {
			throw new PosValidationException("Please enter product name.");
		}
		
		return repo.update(product);
	}

	public List<Product> search(Category category, String name, Integer from, Integer to) {
		
		StringBuffer sb = new StringBuffer("select p from Product p where 1 = 1");
		Map<String, Object> params = new HashMap<>();
		
		if(null != category) {
			sb.append(" and p.category = :category");
			params.put("category", category);
		}
		
		if(null != name && !name.isEmpty()) {
			sb.append(" and upper(p.name) like upper(:name)");
			params.put("name", name.concat("%"));
		}
		
		List<Product> products = repo.select(sb.toString(), params);
		
		Predicate<Product> filter = a -> true;
		
		if(null != from) {
			filter = filter.and(a -> {
				if(null != a.getCuttentPrice()) {
					return a.getCuttentPrice().getPrice() >= from;
				}
				
				return false;
			});
		}
		
		if(null != to) {
			filter = filter.and(a -> {
				if(null != a.getCuttentPrice()) {
					return a.getCuttentPrice().getPrice() <= to;
				}
				
				return false;
			});
		}
		
		return products.stream().filter(filter).collect(Collectors.toList());
	}

	public List<PurchaseItem> getPurchaseByProduct(Product product) {
		return purchaseService.findByProduct(product);
	}

	public Product findByCategoryAndName(Category category, String name) {
		Map<String, Object> params = new HashMap<>();
		params.put("name", name);
		params.put("category", category);
		List<Product> list = repo.selectNamedQuery("Product.findByCategoryAndName", params);
		
		if(list.size() > 0) {
			return list.get(0);
		}
		return null;
	}

}
