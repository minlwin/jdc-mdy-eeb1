package com.jdc.pos.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

import com.jdc.pos.entitty.PurchaseShop;
import com.jdc.pos.repo.PurchaseShopRepo;

@LocalBean
@Stateless
public class PurchaseShopService {
	
	@Inject
	private PurchaseShopRepo repo;

	public PurchaseShop findById(int id) {
		return repo.find(id);
	}

	public List<PurchaseShop> search(String schTownship, String schShopName, String schContactPerson) {
		
		StringBuffer sb = new StringBuffer("select s from PurchaseShop s where 1 = 1");
		Map<String, Object> params = new HashMap<>();
		
		if(null != schTownship && !schTownship.isEmpty()) {
			sb.append(" and lower(s.contact.township) like lower(:townships)");
			params.put("township", schTownship.concat("%"));
		}
		
		if(null != schShopName && !schShopName.isEmpty()) {
			sb.append(" and upper(s.name) like upper(:name)");
			params.put("name", schShopName.concat("%"));
		}

		if(null != schContactPerson && !schContactPerson.isEmpty()) {
			sb.append(" and lower(s.contactPerson) like lower(:person)");
			params.put("person", schContactPerson.concat("%"));
		}

		return repo.select(sb.toString(), params);
	}

	public void save(PurchaseShop shop) {
		repo.update(shop);
	}

	public Optional<PurchaseShop> findByName(String name) {
		Map<String, Object> params = new HashMap<>();
		params.put("name", name);
		
		List<PurchaseShop> list = repo.selectNamedQuery("PurchaseShop.findByName", params);
		
		if(list.size() > 0) {
			return Optional.of(list.get(0));
		}
		
		return Optional.ofNullable(null);
	}

}
