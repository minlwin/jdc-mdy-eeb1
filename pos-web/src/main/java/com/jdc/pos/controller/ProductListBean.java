package com.jdc.pos.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.pos.entitty.Category;
import com.jdc.pos.entitty.Product;
import com.jdc.pos.service.ProductService;

@Named
@ViewScoped
public class ProductListBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Product product;
	private List<Product> list;
	
	private Category category;
	private String name;
	private Integer from;
	private Integer to;
	
	@Inject
	private ProductService service;
	
	public void addNew() {
		product = new Product();
	}
	
	public void edit(Product p) {
		product = p;
	}
	
	public void save() {
		service.save(product);
		search();
	}
	
	public void search() {
		list = service.search(category, name, from, to);
		System.out.println(list.size());
	}
	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public List<Product> getList() {
		return list;
	}

	public void setList(List<Product> list) {
		this.list = list;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getFrom() {
		return from;
	}

	public void setFrom(Integer from) {
		this.from = from;
	}

	public Integer getTo() {
		return to;
	}

	public void setTo(Integer to) {
		this.to = to;
	}

}
