package com.jdc.pos.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.pos.entitty.Employee;
import com.jdc.pos.entitty.Employee.Role;
import com.jdc.pos.service.EmployeeService;

@Named
@ViewScoped
public class EmployeeBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String name;
	private Role role;
	
	private Employee employee;
	private List<Employee> list;
	
	@Inject
	private EmployeeService service;
	
	public void addNew() {
		employee = new Employee();
	}
	
	public void save() {
		service.save(employee);
		search();
	}
	
	public void editEmployee(Employee emp) {
		employee = emp;
	}
	
	public void search() {
		list = service.search(role, name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public List<Employee> getList() {
		return list;
	}

	public void setList(List<Employee> list) {
		this.list = list;
	}

	public EmployeeService getService() {
		return service;
	}

	public void setService(EmployeeService service) {
		this.service = service;
	}
	
	public Role[] getRoles() {
		return Role.values();
	}

}
