package com.jdc.pos.controller.converters;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.pos.entitty.Category;
import com.jdc.pos.service.CategoryService;

@Named
@RequestScoped
public class CategoryConverter implements Converter {
	
	@Inject
	private CategoryService service;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		
		try {
			
			int id = Integer.parseInt(value);
			return service.findById(id);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		
		if(null != value) {
			Category cat = (Category) value;
			return cat.getId().toString();
		}
		
		return null;
	}

}
