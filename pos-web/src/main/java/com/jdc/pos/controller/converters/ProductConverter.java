package com.jdc.pos.controller.converters;

import javax.enterprise.context.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.pos.entitty.Product;
import com.jdc.pos.service.ProductService;

@Named
@RequestScoped
public class ProductConverter implements Converter{
	
	@Inject
	private ProductService service;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		
		try {
			
			int id = Integer.parseInt(value);
			return service.findById(id);
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		
		if(null != value) {
			Product item = (Product) value;
			return item.getId().toString();
		}
		
		return null;
	}


}
