package com.jdc.pos.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import com.jdc.pos.entitty.Category;
import com.jdc.pos.entitty.Product;
import com.jdc.pos.service.CategoryService;
import com.jdc.pos.service.ProductService;

@Model
public class PublicProductsBean {

	private Category category;
	private List<Product> products;
	
	@Inject
	private CategoryService catService;
	@Inject
	private ProductService prodService;
	
	@PostConstruct
	private void init() {
		
		String str = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("cat");
		if(null != str && !str.isEmpty()) {
			int id = Integer.parseInt(str);
			category = catService.findById(id);
			
			products = prodService.findByCategory(category);
		}
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
	
}
