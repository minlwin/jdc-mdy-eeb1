package com.jdc.pos.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.pos.MessageHandler;
import com.jdc.pos.PosValidationException;
import com.jdc.pos.entitty.Invoice;
import com.jdc.pos.entitty.PaidHistory;
import com.jdc.pos.entitty.PurchaseItem;
import com.jdc.pos.entitty.PurchaseShop;
import com.jdc.pos.service.InvoiceService;
import com.jdc.pos.service.PurchaseShopService;

@Named
@ViewScoped
public class PurchaseEditBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Invoice invoice;
	private PurchaseShop shop;
	private PurchaseItem item;
	private PaidHistory paid;
	
	private List<PaidHistory> paidList;

	@Inject
	private InvoiceService service;

	@Inject
	private PurchaseShopService shopService;

	@PostConstruct
	private void init() {
		
		shop = new PurchaseShop();
		item = new PurchaseItem();
		paid = new PaidHistory();

		String str = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap().get("id");
		
		if(null != str && !str.isEmpty()) {
			long id = Long.parseLong(str);
			invoice = service.findById(id);
			
			paidList = service.findByInvoiceId(id);
			shop = invoice.getShop();
			
			calculate();
		}
		
		if(null == invoice) {
			invoice = new Invoice();
			paidList = new ArrayList<>();
		}
	}

	public void searchShop() {
		Optional<PurchaseShop> shopOption = shopService.findByName(shop.getName());
		shopOption.ifPresent(s -> shop = s);
	}

	public void addItem() {
		invoice.addItem(item);
		item = new PurchaseItem();
		calculate();
	}

	public void deleteItem(PurchaseItem item) {
		invoice.removeItem(item);
		calculate();
	}
	
	@MessageHandler
	public void addPaid() {
		
		if(paidList.stream().filter(a -> a.getId().getRefDate().equals(paid.getId().getRefDate())).findAny().isPresent()) {
			throw new PosValidationException("You already have paid in this day. Please select other day.");
		}
		
		LocalDate lastPaidDate = paidList.stream()
				.map(a -> a.getId().getRefDate())
				.sorted((a, b) -> b.compareTo(a))
				.findFirst().orElse(null);
		
		if(null != lastPaidDate && lastPaidDate.isAfter(paid.getId().getRefDate())) {
			throw new PosValidationException("Paid date must be greater than last paid date.");
		}
		
		paidList.add(paid);
		paid = new PaidHistory();
		calculate();
	}
	
	public void deletePaid(PaidHistory paid) {
		
		Iterator<PaidHistory> itr = paidList.iterator();
		
		while(itr.hasNext()) {
			PaidHistory old = itr.next();
			
			if(old.getId().equals(paid.getId())) {
				itr.remove();
				break;
			}
		}
		
		calculate();
	}
	
	private void calculate() {

		List<PaidHistory> temp = new ArrayList<>(paidList);
		paidList.clear();
		
		for(PaidHistory p : temp) {
			p.setLastRemain(getRemainAmount());
			paidList.add(p);
		}
		
		paid.setLastRemain(getRemainAmount());
	}

	public int getPaidAmount() {
		return paidList.stream().mapToInt(a -> a.getPaid()).sum();
	}
	
	public int getRemainAmount() {
		return (null == invoice.getTotal() ? 0 : invoice.getTotal()) - getPaidAmount();
	}

	public String save() {
		invoice.setShop(shop);
		service.save(invoice, paidList);
		return "/admin/purchases?faces-redirect=true";
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public PurchaseShop getShop() {
		return shop;
	}

	public void setShop(PurchaseShop shop) {
		this.shop = shop;
	}

	public PurchaseItem getItem() {
		return item;
	}

	public void setItem(PurchaseItem item) {
		this.item = item;
	}

	public PaidHistory getPaid() {
		return paid;
	}

	public void setPaid(PaidHistory paid) {
		this.paid = paid;
	}

	public List<PaidHistory> getPaidList() {
		return paidList;
	}

	public void setPaidList(List<PaidHistory> paidList) {
		this.paidList = paidList;
	}

}
