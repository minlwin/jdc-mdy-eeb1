package com.jdc.pos.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.pos.entitty.Category;
import com.jdc.pos.service.CategoryService;

@Named
@ViewScoped
public class CategoryBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private Category category;
	private List<Category> list;
	
	@Inject
	private CategoryService service;
	
	public void edit(Category c) {
		this.category = c;
	}
	
	public void addNew() {
		this.category = new Category();
	}
	
	public void search() {
		list = service.search(name);
	}
	
	public void save() {
		service.save(category);
		search();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<Category> getList() {
		return list;
	}

	public void setList(List<Category> list) {
		this.list = list;
	}
	
	
}
