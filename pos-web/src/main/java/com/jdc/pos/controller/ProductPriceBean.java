package com.jdc.pos.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.pos.MessageHandler;
import com.jdc.pos.entitty.Product;
import com.jdc.pos.entitty.PurchaseItem;
import com.jdc.pos.entitty.SellPrice;
import com.jdc.pos.service.ProductService;

@Named
@ViewScoped
public class ProductPriceBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Product product;
	private SellPrice price;
	
	private List<PurchaseItem> purchases;
	
	@Inject
	private ProductService service;
	
	@PostConstruct
	private void init() {
		String str = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap().get("product");
		
		if(null != str && !str.isEmpty()) {
			int id = Integer.parseInt(str);
			product = service.findById(id);
			
			purchases = service.getPurchaseByProduct(product);
		}
		
		
	}
	
	public void addPrice() {
		price = new SellPrice();
		product.addPrice(price);
		price.getId().setRefDate(LocalDate.now().plusDays(1));
	}
	
	@MessageHandler
	public void savePrice() {
		
		service.save(product);
		product = service.findById(product.getId());
	}
	
	@MessageHandler
	public void editPrice(SellPrice price) {
		
		this.price = price;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public SellPrice getPrice() {
		return price;
	}

	public void setPrice(SellPrice price) {
		this.price = price;
	}

	public List<PurchaseItem> getPurchases() {
		return purchases;
	}

	public void setPurchases(List<PurchaseItem> purchases) {
		this.purchases = purchases;
	}

}
