package com.jdc.pos.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.pos.entitty.Category;
import com.jdc.pos.service.CategoryService;

@Named
@RequestScoped
public class IndexBean {
	
	private List<Category> categories;
	
	@Inject
	private CategoryService service;
	
	@PostConstruct
	public void init() {
		categories = service.findForIndexPage();
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
}
