package com.jdc.pos.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.pos.entitty.Category;
import com.jdc.pos.entitty.StockAction;
import com.jdc.pos.service.InventorySerrvice;

@Named
@ViewScoped
public class InventoryBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Category category;
	private String productName;
	private LocalDate dateFrom;
	private LocalDate dateTo;
	
	private List<StockAction> list;
	
	@Inject
	private InventorySerrvice service;
	
	public void search() {
		list = service.search(category, productName, dateFrom, dateTo);
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}

	public LocalDate getDateTo() {
		return dateTo;
	}

	public void setDateTo(LocalDate dateTo) {
		this.dateTo = dateTo;
	}

	public List<StockAction> getList() {
		return list;
	}

	public void setList(List<StockAction> list) {
		this.list = list;
	}
	
	

}
