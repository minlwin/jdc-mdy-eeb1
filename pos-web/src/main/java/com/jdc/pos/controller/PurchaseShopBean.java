package com.jdc.pos.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.pos.entitty.PurchaseShop;
import com.jdc.pos.service.PurchaseShopService;

@Named
@ViewScoped
public class PurchaseShopBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String schTownship;
	private String schShopName;
	private String schContactPerson;
	
	private List<PurchaseShop> list;
	
	private PurchaseShop shop;
	
	@Inject
	private PurchaseShopService service;
	
	public void addNew() {
		shop = new PurchaseShop();
	}
	
	public void edit(PurchaseShop shop) {
		this.shop = shop;
	}
	
	public void search() {
		list = service.search(schTownship, schShopName, schContactPerson);
	}
	
	public void save() {
		service.save(shop);
		search();
	}

	public String getSchTownship() {
		return schTownship;
	}

	public void setSchTownship(String schTownship) {
		this.schTownship = schTownship;
	}

	public String getSchShopName() {
		return schShopName;
	}

	public void setSchShopName(String schShopName) {
		this.schShopName = schShopName;
	}

	public String getSchContactPerson() {
		return schContactPerson;
	}

	public void setSchContactPerson(String schContactPerson) {
		this.schContactPerson = schContactPerson;
	}

	public List<PurchaseShop> getList() {
		return list;
	}

	public void setList(List<PurchaseShop> list) {
		this.list = list;
	}

	public PurchaseShop getShop() {
		return shop;
	}

	public void setShop(PurchaseShop shop) {
		this.shop = shop;
	}
	
	

}
