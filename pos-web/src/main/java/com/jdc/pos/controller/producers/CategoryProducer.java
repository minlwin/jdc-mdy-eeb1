package com.jdc.pos.controller.producers;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.pos.entitty.Category;
import com.jdc.pos.service.CategoryService;

@ApplicationScoped
public class CategoryProducer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Named
	@Produces
	private List<Category> categories;
	
	@Inject
	private CategoryService service;
	
	@PostConstruct
	private void init() {
		load(null);
	}
	
	public void load(@Observes Category c) {
		categories = service.search(null);
	}
	

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

}
