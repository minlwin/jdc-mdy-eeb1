package com.jdc.pos.controller;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.pos.entitty.Invoice;
import com.jdc.pos.service.InvoiceService;

@Named
@ViewScoped
public class PurchaseListBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// search
	private String schShop;
	private LocalDate dateFrom;
	private LocalDate dateTo;
	
	// result list
	private List<Invoice> list;
	
	@Inject
	private InvoiceService service;
	
	@PostConstruct
	private void init() {
		dateFrom = LocalDate.now().withDayOfMonth(1);
		dateTo = LocalDate.now();
	}
	
	public void search() {
		list = service.search(schShop, dateFrom, dateTo);
	}

	public String getSchShop() {
		return schShop;
	}

	public void setSchShop(String schShop) {
		this.schShop = schShop;
	}

	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}

	public LocalDate getDateTo() {
		return dateTo;
	}

	public void setDateTo(LocalDate dateTo) {
		this.dateTo = dateTo;
	}

	public List<Invoice> getList() {
		return list;
	}

	public void setList(List<Invoice> list) {
		this.list = list;
	}
	
	

}
