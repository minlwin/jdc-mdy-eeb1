package com.jdc.pos;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class PosValidationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PosValidationException(String message) {
		super(message);
	}

}
