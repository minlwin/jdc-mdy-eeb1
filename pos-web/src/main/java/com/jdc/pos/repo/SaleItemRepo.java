package com.jdc.pos.repo;

import com.jdc.pos.entitty.SaleItem;

public class SaleItemRepo extends AbstractRepository<SaleItem>{

	public SaleItemRepo() {
		super(SaleItem.class);
	}

}
