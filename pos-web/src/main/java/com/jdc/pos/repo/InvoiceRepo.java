package com.jdc.pos.repo;

import com.jdc.pos.entitty.Invoice;

public class InvoiceRepo extends AbstractRepository<Invoice>{

	public InvoiceRepo() {
		super(Invoice.class);
	}

}
