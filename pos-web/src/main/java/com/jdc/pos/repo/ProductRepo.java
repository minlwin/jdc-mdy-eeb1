package com.jdc.pos.repo;

import com.jdc.pos.entitty.Product;

public class ProductRepo extends AbstractRepository<Product>{

	public ProductRepo() {
		super(Product.class);
	}
}
