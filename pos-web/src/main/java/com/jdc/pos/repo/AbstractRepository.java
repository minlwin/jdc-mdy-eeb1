package com.jdc.pos.repo;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

public abstract class AbstractRepository<T> {

	@PersistenceContext
	private EntityManager em;

	private Class<T> type;

	public AbstractRepository(Class<T> type) {
		super();
		this.type = type;
	}

	public void create(T t) {
		em.persist(t);
	}

	public T update(T t) {
		return em.merge(t);
	}

	public void delete(T t) {
		t = em.merge(t);
		em.remove(t);
	}

	public T find(Object id) {
		return em.find(type, id);
	}
	
	public List<T> select(String query, Map<String, Object> params) {
		
		TypedQuery<T> q = em.createQuery(query, type);
		
		return select(q, params, 0, 0);

	}
	
	public List<T> select(String query, Map<String, Object> params, int start, int limit) {
		
		TypedQuery<T> q = em.createQuery(query, type);
		return select(q, params, start, limit);
	}
	
	public List<T> selectNamedQuery(String query, Map<String, Object> params) {
		
		TypedQuery<T> q = em.createNamedQuery(query, type);
		
		return select(q, params, 0, 0);
	}
	
	public List<T> selectNamedQuery(String query, Map<String, Object> params, int start, int limit) {
		
		TypedQuery<T> q = em.createNamedQuery(query, type);
		
		return select(q, params, start, limit);

	}
	
	public long selectCount(String query, Map<String, Object> params) {
		
		TypedQuery<Long> q = em.createQuery(query, Long.class);
		
		return selectCount(q, params);
	}
	
	public long selectCountByNamedQuery(String query, Map<String, Object> params) {
		
		TypedQuery<Long> q = em.createNamedQuery(query, Long.class);
		
		return selectCount(q, params);
	}
	
	private long selectCount(TypedQuery<Long> q, Map<String, Object> params) {
		if(null != params) {
			for(String key : params.keySet()) {
				q.setParameter(key, params.get(key));
			}
		}
		
		return q.getSingleResult();
	}
	
	private List<T> select(TypedQuery<T> q, Map<String, Object> params, int start, int limit) {
		if(null != params) {
			for(String key : params.keySet()) {
				q.setParameter(key, params.get(key));
			}
		}

		if(start > 0 && limit > 0) {
			q.setFirstResult(start);
			q.setMaxResults(limit);
		}
		
		return q.getResultList();		
	}
}
