package com.jdc.pos.repo;

import com.jdc.pos.entitty.Inventory;

public class InventoryRepo extends AbstractRepository<Inventory>{

	public InventoryRepo() {
		super(Inventory.class);
	}

}
