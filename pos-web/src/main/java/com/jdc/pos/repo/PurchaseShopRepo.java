package com.jdc.pos.repo;

import com.jdc.pos.entitty.PurchaseShop;

public class PurchaseShopRepo extends AbstractRepository<PurchaseShop>{

	public PurchaseShopRepo() {
		super(PurchaseShop.class);
	}

}
