package com.jdc.pos.repo;

import com.jdc.pos.entitty.PaidHistory;

public class PaidHistoryRepo extends AbstractRepository<PaidHistory>{

	public PaidHistoryRepo() {
		super(PaidHistory.class);
	}

}
