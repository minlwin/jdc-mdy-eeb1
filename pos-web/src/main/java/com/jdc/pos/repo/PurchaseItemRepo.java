package com.jdc.pos.repo;

import com.jdc.pos.entitty.PurchaseItem;

public class PurchaseItemRepo extends AbstractRepository<PurchaseItem> {

	public PurchaseItemRepo() {
		super(PurchaseItem.class);
	}
}
