package com.jdc.pos.repo;

import com.jdc.pos.entitty.Employee;

public class EmployeeRepo extends AbstractRepository<Employee>{

	public EmployeeRepo() {
		super(Employee.class);
	}
}
