package com.jdc.pos.repo;

import com.jdc.pos.entitty.SellPrice;

public class SellPriceRepo extends AbstractRepository<SellPrice>{

	public SellPriceRepo() {
		super(SellPrice.class);
	}

}
