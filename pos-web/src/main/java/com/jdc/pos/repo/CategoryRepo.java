package com.jdc.pos.repo;

import com.jdc.pos.entitty.Category;

public class CategoryRepo extends AbstractRepository<Category> {

	public CategoryRepo() {
		super(Category.class);
	}
}
