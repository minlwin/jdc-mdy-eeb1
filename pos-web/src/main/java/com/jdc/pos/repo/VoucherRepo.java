package com.jdc.pos.repo;

import com.jdc.pos.entitty.Voucher;

public class VoucherRepo extends AbstractRepository<Voucher>{

	public VoucherRepo() {
		super(Voucher.class);
	}

}
