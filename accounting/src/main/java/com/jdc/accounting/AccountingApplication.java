package com.jdc.accounting;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class AccountingApplication extends Application {

}
