package com.jdc.accounting.beans;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.accounting.entity.Balance;
import com.jdc.accounting.entity.BalanceDetails;
import com.jdc.accounting.entity.Category;
import com.jdc.accounting.entity.Category.Type;
import com.jdc.accounting.entity.Member;
import com.jdc.accounting.producers.CategoryProducer;
import com.jdc.accounting.service.BalanceService;

@Named
@ViewScoped
public class BalanceEditBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Balance balance;

	@Inject
	@Named
	private Member loginUser;

	private String title;
	
	@Inject
	private BalanceService service;
	@Inject
	private CategoryProducer catProducer;

	@PostConstruct
	private void init() {
		title = "Add New Balance";
		balance = new Balance();
		balance.addDetails(new BalanceDetails());
		balance.setMember(loginUser);
		
		String str = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap().get("id");
		
		if(null != str) {
			int id = Integer.parseInt(str);
			balance = service.findById(id);
			title = "Edit Balance";
		}
	}

	public String save() {
		service.save(balance);
		return "/views/member/balances?faces-redirect=true";
	}

	public void addDetails() {
		balance.addDetails(new BalanceDetails());
	}

	public void removeChecks() {
		
		List<BalanceDetails> toRemoveList = balance.getDetailsList()
				.stream().filter(a -> a.isCheck())
				.collect(Collectors.toList());
		
		toRemoveList.forEach(d -> balance.removeDetails(d));
		
		if(balance.getDetailsList().isEmpty()) {
			addDetails();
		}
	}
	
	public List<Category> getCategories() {
		Type type = balance.getType();
		return catProducer.findByType(type);
	}

	public Balance getBalance() {
		return balance;
	}

	public void setBalance(Balance balance) {
		this.balance = balance;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
