package com.jdc.accounting.beans;

import javax.enterprise.event.Event;
import javax.enterprise.inject.Model;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.jdc.accounting.entity.Member;
import com.jdc.accounting.service.MemberService;

@Model
public class SecurityBean {

	private String name;
	private String loginId;
	private String password;

	@Inject
	private MemberService service;
	
	@Inject
	private Event<Member> loginEvent;

	public String signUp() {
		service.createMember(name, loginId, password);
		return signIn();
	}

	public String signIn() {

		try {
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
					.getRequest();

			request.login(loginId, password);
			
			Member member = new Member();
			member.setLoginId(loginId);
			
			loginEvent.fire(member);

			if (request.isUserInRole("Admin")) {
				return "/views/admin/home";
			}

			return "/views/member/home";

		} catch (Exception e) {
			FacesMessage message = new FacesMessage("Please check Login Informations.");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}

		return null;
	}

	public String signOut() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "/index?faces-redirect=true";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
