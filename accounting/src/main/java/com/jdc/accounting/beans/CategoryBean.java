package com.jdc.accounting.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.accounting.entity.Category;
import com.jdc.accounting.service.CategoryService;

@Named
@ViewScoped
public class CategoryBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Category category;

	@Inject
	private CategoryService service;

	@Inject
	private Event<Category> event;
	
	@PostConstruct
	private void init() {
		category = new Category();
	}

	public void save() {
		service.save(category);
		event.fire(category);
		category = new Category();
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

}
