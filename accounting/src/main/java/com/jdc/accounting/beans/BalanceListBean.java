package com.jdc.accounting.beans;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.accounting.entity.BalanceDetails;
import com.jdc.accounting.entity.Category;
import com.jdc.accounting.entity.Category.Type;
import com.jdc.accounting.producers.CategoryProducer;
import com.jdc.accounting.service.BalanceService;

@Named
@ViewScoped
public class BalanceListBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Type type;
	private Category category;
	private LocalDate dateFrom;
	private LocalDate dateTo;

	private List<BalanceDetails> list;
	
	@Inject
	private CategoryProducer catProducer;
	
	@Inject
	private BalanceService service;
	
	public void search() {
		list = service.search(type, category, dateFrom, dateTo);
	}
	
	public void delete(int id) {
		service.delete(id);
		search();
	}
	
	public List<Category> getCategories() {
		return catProducer.findByType(type);
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(LocalDate dateFrom) {
		this.dateFrom = dateFrom;
	}

	public LocalDate getDateTo() {
		return dateTo;
	}

	public void setDateTo(LocalDate dateTo) {
		this.dateTo = dateTo;
	}

	public List<BalanceDetails> getList() {
		return list;
	}

	public void setList(List<BalanceDetails> list) {
		this.list = list;
	}

}
