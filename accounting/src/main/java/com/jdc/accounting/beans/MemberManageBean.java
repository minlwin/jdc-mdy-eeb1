package com.jdc.accounting.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.accounting.entity.Member;
import com.jdc.accounting.entity.Member.Role;
import com.jdc.accounting.service.MemberService;

@Named
@ViewScoped
public class MemberManageBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Role schRole;
	private String schName;
	
	private List<Member> list;
	
	@Inject
	private MemberService service;
	
	public void search() {
		list = service.search(schRole, schName);
	}
	
	public Role getSchRole() {
		return schRole;
	}

	public void setSchRole(Role schRole) {
		this.schRole = schRole;
	}

	public String getSchName() {
		return schName;
	}

	public void setSchName(String schName) {
		this.schName = schName;
	}

	public List<Member> getList() {
		return list;
	}

	public void setList(List<Member> list) {
		this.list = list;
	}

}
