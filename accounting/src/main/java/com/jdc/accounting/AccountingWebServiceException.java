package com.jdc.accounting;

public class AccountingWebServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AccountingWebServiceException(String message) {
		super(message);
	}
	
}
