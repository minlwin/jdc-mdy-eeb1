package com.jdc.accounting.service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.jdc.accounting.entity.Balance;
import com.jdc.accounting.entity.BalanceDetails;
import com.jdc.accounting.entity.Category;
import com.jdc.accounting.entity.Category.Type;

@Stateless
@LocalBean
public class BalanceService {
	
	@PersistenceContext
	private EntityManager em;

	public void save(Balance balance) {
		if(balance.getId() == 0) {
			em.persist(balance);
		} else {
			em.merge(balance);
		}
	}

	public Balance findById(int id) {
		return em.find(Balance.class, id);
	}

	public List<BalanceDetails> search(Type type, Category category, LocalDate dateFrom, LocalDate dateTo) {
		
		StringBuffer sb = new StringBuffer("select b from BalanceDetails b where 1 = 1");
		
		Map<String, Object> params = new HashMap<>();
		
		if(null != category) {
			sb.append(" and b.category = :category");
			params.put("category", category);
		} else {
			if(null != type) {
				sb.append(" and b.balance.type = :type");
				params.put("type", type);
			}
		}
		
		if(null != dateFrom) {
			sb.append(" and b.balance.refDate >= :dateFrom");
			params.put("dateFrom", dateFrom);
		}
		
		if(null != dateTo) {
			sb.append(" and b.balance.refDate <= :dateTo");
			params.put("dateTo", dateTo);
		}
		
		TypedQuery<BalanceDetails> q = em.createQuery(sb.toString(), BalanceDetails.class);
		
		for(String key : params.keySet()) {
			q.setParameter(key, params.get(key));
		}
		
		List<BalanceDetails> list = q.getResultList();
		
		if(!list.isEmpty()) {
			StringBuffer sbBalance = new StringBuffer("select sum(b.amount) from BalanceDetails b where 1 = 1");
			Map<String, Object> paramsB = new HashMap<>();

			int firstId = list.get(0).getId();
			sbBalance.append(" and b.id < :lastId");
			paramsB.put("lastId", firstId);
			paramsB.put("type", Type.Income);
			// find income
			int incomes = findTotal(sbBalance.toString() + " and b.balance.type = :type", paramsB);
			
			// find expense
			int expenses = findTotal(sbBalance.toString() + " and b.balance.type != :type", paramsB);
			
			int total = incomes - expenses;
			
			for(BalanceDetails d : list) {
				if(d.getBalance().getType() == Type.Income) {
					total += d.getAmount();
				} else {
					total -= d.getAmount();
				}
				
				d.setTotal(total);
			}
			
		}
		
		return list;
	}

	private int findTotal(String sql, Map<String, Object> params) {
		
		TypedQuery<Long> q = em.createQuery(sql, Long.class);
		
		for(String key : params.keySet()) {
			q.setParameter(key, params.get(key));
		}
		
		Long longValue = q.getSingleResult();
		
		return null == longValue ? 0 : longValue.intValue();
	}

	public void delete(int id) {
		BalanceDetails details = em.find(BalanceDetails.class, id);
		Balance balance = details.getBalance();
		balance.getDetailsList().remove(details);
		details.setBalance(null);
		em.remove(details);
		
		if(balance.getDetailsList().isEmpty()) {
			em.remove(balance);
		}
	}

}
