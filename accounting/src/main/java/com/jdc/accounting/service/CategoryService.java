package com.jdc.accounting.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.jdc.accounting.entity.Category;
import com.jdc.accounting.entity.Category.Type;

@Stateless
@LocalBean
public class CategoryService {

	@PersistenceContext
	private EntityManager em;

	public void save(Category category) {
		if (category.getId() > 0) {
			em.merge(category);
		} else {
			em.persist(category);
		}
	}

	public List<Category> getAll() {
		return em.createNamedQuery("Category.getAll", Category.class).getResultList();
	}

	public Category findById(int id) {
		return em.find(Category.class, id);
	}

	public List<Category> findByType(Type type) {
		return em.createNamedQuery("Category.findByType", Category.class)
				.setParameter("type", type)
				.getResultList();
	}

	public List<Category> findByName(String name) {
		return em.createNamedQuery("Category.findByName", Category.class)
				.setParameter("name", name.concat("%").toLowerCase())
				.getResultList();
	}

	public void delete(int id) {
		em.remove(em.find(Category.class, id));
	}

}
