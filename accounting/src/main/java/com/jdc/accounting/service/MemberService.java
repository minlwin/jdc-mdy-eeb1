package com.jdc.accounting.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.jdc.accounting.entity.Member;
import com.jdc.accounting.entity.Member.Role;

@Stateless
@LocalBean
public class MemberService {
	
	@PersistenceContext
	private EntityManager em;

	public void createMember(String name, String loginId, String password) {
		
		Member member = new Member();
		member.setName(name);
		member.setLoginId(loginId);
		member.setPassword(encript(password));
		member.setRole(Role.Member);
		
		em.persist(member);
	}

	private static String encript(String password) {
		
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte [] data = digest.digest(password.getBytes());
			return Base64.getEncoder().encodeToString(data);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		System.out.println(encript("admin"));
	}

	public List<Member> search(Role schRole, String schName) {
		
		StringBuffer sb = new StringBuffer("select m from Member m where 1 = 1");
		Map<String, Object> params = new HashMap<>();
		
		if(null != schRole) {
			sb.append(" and m.role = :role");
			params.put("role", schRole);
		}
		
		if(null != schName && !schName.isEmpty()) {
			sb.append(" and upper(m.name) like upper(:name) ");
			params.put("name", schName.concat("%"));
		}
		
		TypedQuery<Member> q = em.createQuery(sb.toString(), Member.class);
		
		for(String key : params.keySet()) {
			q.setParameter(key, params.get(key));
		}
		
		return q.getResultList();
	}

	public Member findById(String loginId) {
		return em.find(Member.class, loginId);
	}

}
