package com.jdc.accounting.converter;

import javax.enterprise.inject.Model;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Inject;

import com.jdc.accounting.entity.Category;
import com.jdc.accounting.service.CategoryService;

@Model
public class CategoryConverter implements Converter {
	
	@Inject
	private CategoryService service;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {

		try {
			if(null != value && !value.isEmpty()) {
				int id = Integer.parseInt(value);
				return service.findById(id);
			}
		} catch (NumberFormatException e) {
			// Nothing to do
		}
		
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		
		if(null != value) {
			Category c = (Category) value;
			return String.valueOf(c.getId());
		}
		
		return null;
	}

}
