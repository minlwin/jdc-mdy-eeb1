package com.jdc.accounting.resources;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.jdc.accounting.entity.Member;
import com.jdc.accounting.entity.Member.Role;
import com.jdc.accounting.service.MemberService;

@Path("/members")
@Produces(MediaType.APPLICATION_JSON)
public class MemberResources {
	
	@Inject
	private MemberService service;

	@GET
	public List<Member> getAllMembers() {
		return service.search(null, null);
	}
	
	@GET
	@Path("/role/{role:[a-zA-Z]*}")
	public List<Member> findByRole(@PathParam("role") Role role) {
		return service.search(role, null);
	}
	
	@GET
	@Path("/name/{name}")
	@Produces(MediaType.APPLICATION_XML)
	public List<Member> findByName(@PathParam("name") String name) {
		return service.search(null, name);
	}
	
	@GET
	@Path("/{id}")
	public Member findById(@PathParam("id") String id) {
		return service.findById(id);
	}
}
