package com.jdc.accounting.resources;

import java.net.URI;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.jdc.accounting.AccountingWebServiceException;
import com.jdc.accounting.entity.Category;
import com.jdc.accounting.entity.Category.Type;
import com.jdc.accounting.service.CategoryService;

@Path("/categories")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategoryResources {
	
	@Inject
	private CategoryService service;

	@Context
	private UriInfo uriInfo;
	
	@GET
	public List<Category> getAll() {
		return service.getAll();
	}
	
	@GET
	@Path("{id:\\d+}")
	public Response findById(@PathParam("id") int id) {
		return Response.ok(service.findById(id)).build();
	}
	
	@GET
	@Path("/type/{type}")
	public List<Category> findByType(@PathParam("type") Type type) {
		return service.findByType(type);
	}
	
	@GET
	@Path("/name")
	public List<Category> findByName(
		@QueryParam("name") 
		@DefaultValue("S") String name) {
		return service.findByName(name);
	}
	
	@POST
	public Response create(Category category) {
		
		service.save(category);
		URI uri = uriInfo.getRequestUriBuilder()
				.path(String.valueOf(category.getId())).build();
		System.out.println(uri.getPath());
		
		return Response.created(uri).build();
	}
	
	@PUT
	public Response update(Category category) {
		
		if(null == category.getName() || category.getName().isEmpty()) {
			throw new WebApplicationException("You must set name.", Response.Status.BAD_REQUEST);
		}
		
		service.save(category);
		return Response.accepted(category).build();
	}
	
	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") @DefaultValue("0") int id) {
		
		if(id == 0) {
			throw new AccountingWebServiceException("There is no data to delete.");
		}
		
		service.delete(id);
		return Response.noContent().build();
	}

}
