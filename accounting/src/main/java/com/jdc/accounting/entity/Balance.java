package com.jdc.accounting.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.jdc.accounting.entity.Category.Type;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.OneToMany;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.FetchType.EAGER;

@Entity
public class Balance implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;
	private Type type;
	private LocalDate refDate;

	@OneToMany(mappedBy = "balance", cascade = { PERSIST, MERGE }, orphanRemoval = true, fetch = EAGER)
	private List<BalanceDetails> detailsList;
	
	@ManyToOne
	private Member member;
	
	public Balance() {
		detailsList = new ArrayList<>();
	}

	public List<BalanceDetails> getDetailsList() {
		return detailsList;
	}
	
	public void addDetails(BalanceDetails d) {
		d.setBalance(this);
		detailsList.add(d);
	}
	
	public void removeDetails(BalanceDetails d) {
		d.setBalance(null);
		detailsList.remove(d);
	}

	public void setDetailsList(List<BalanceDetails> detailsList) {
		this.detailsList = detailsList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public LocalDate getRefDate() {
		return refDate;
	}

	public void setRefDate(LocalDate refDate) {
		this.refDate = refDate;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((member == null) ? 0 : member.hashCode());
		result = prime * result + ((refDate == null) ? 0 : refDate.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Balance other = (Balance) obj;
		if (id != other.id)
			return false;
		if (member == null) {
			if (other.member != null)
				return false;
		} else if (!member.equals(other.member))
			return false;
		if (refDate == null) {
			if (other.refDate != null)
				return false;
		} else if (!refDate.equals(other.refDate))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

}
