package com.jdc.accounting.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@NamedQueries({
	@NamedQuery(name="Category.getAll", query="select c from Category c"),
	@NamedQuery(name="Category.findByType", query="select c from Category c where c.type = :type"),
	@NamedQuery(name="Category.findByName", query="select c from Category c where lower(c.name) like :name")	
})
@XmlRootElement
public class Category implements Serializable {

	private static final long serialVersionUID = 1L;

	public enum Type {
		Income, Expense
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;
	@NotEmpty(message="Please enter Category Name.")
	private String name;
	@NotNull(message="Please select one Type for Category.")
	private Type type;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

}
