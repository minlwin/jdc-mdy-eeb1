package com.jdc.accounting.producers;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.accounting.entity.Member;
import com.jdc.accounting.service.MemberService;

@SessionScoped
public class LoginUserProducer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Named
	@Produces
	private Member loginUser;
	
	@Inject
	private MemberService service;
	
	public void login(@Observes Member member) {
		loginUser = service.findById(member.getLoginId());
	}
}
