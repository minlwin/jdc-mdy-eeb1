package com.jdc.accounting.producers;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.accounting.entity.Category;
import com.jdc.accounting.entity.Category.Type;
import com.jdc.accounting.service.CategoryService;

@ApplicationScoped
public class CategoryProducer {

	@Named
	@Produces
	private List<Category> categories;
	
	@Inject
	private CategoryService service;
	
	@PostConstruct
	private void postConstruct() {
		load(null);
	}
	
	public void load(@Observes Category c) {
		categories = service.getAll();
	}

	public List<Category> findByType(Type type) {
		return categories.stream()
				.filter(a -> a.getType() == type)
				.collect(Collectors.toList());
	}
}
