package com.jdc.accounting.producers;

import javax.enterprise.inject.Produces;
import javax.inject.Named;

import com.jdc.accounting.entity.Category.Type;
import com.jdc.accounting.entity.Member.Role;

public class CommonProducer {

	@Named
	@Produces
	public Type[] getTypes() {
		return Type.values();
	}
	
	@Named
	@Produces
	public Role[] getRoles() {
		return Role.values();
	}
}
