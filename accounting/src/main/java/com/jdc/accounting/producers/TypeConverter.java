package com.jdc.accounting.producers;

import javax.enterprise.inject.Model;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.jdc.accounting.entity.Category.Type;

@Model
public class TypeConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {

		try {
			if (null != value && !value.isEmpty()) {
				return Type.valueOf(value);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {

		if (null != value) {
			Type type = (Type) value;
			return type.toString();
		}

		return null;
	}

}
