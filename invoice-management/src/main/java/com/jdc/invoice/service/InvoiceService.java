package com.jdc.invoice.service;

import java.util.List;
import java.util.Map.Entry;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.jdc.invoice.entity.Invoice;

@Stateless
@LocalBean
public class InvoiceService {
	
	@PersistenceContext
	private EntityManager em;

	public void save(Invoice invoice) {
		if(invoice.getId() == 0) {
			em.persist(invoice);
		} else {
			em.merge(invoice);
		}
	}

	public List<Invoice> search(InvoiceSearch search) {
		
		TypedQuery<Invoice> query = em.createQuery(search.getQuery(), Invoice.class);
		
		for(Entry<String, Object> entry : search.getParams().entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		
		return query.getResultList();
	}

}
