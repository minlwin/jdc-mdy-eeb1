package com.jdc.invoice.service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class InvoiceSearch implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private String phone;
	private double priceFrom;
	private double priceTo;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public double getPriceFrom() {
		return priceFrom;
	}

	public void setPriceFrom(double priceFrom) {
		this.priceFrom = priceFrom;
	}

	public double getPriceTo() {
		return priceTo;
	}

	public void setPriceTo(double priceTo) {
		this.priceTo = priceTo;
	}
	
	public String getQuery() {
				
		StringBuffer sb = new StringBuffer("select i from Invoice i where 1 = 1");
		
		if(null != name && !name.isEmpty()) {
			sb.append(" and i.customer like :name");
		}
		
		if(null != phone && !phone.isEmpty()) {
			sb.append(" and i.phone like :phone");
		}

		if(priceFrom > 0) {
			sb.append(" and i.subTotal >= :from");
		}

		if(priceTo > 0) {
			sb.append(" and i.subTotal <= :to");
		}

		return sb.toString();
	}
	
	public Map<String, Object> getParams() {
		
		Map<String, Object> params = new HashMap<>();
			
		if(null != name && !name.isEmpty()) {
			params.put("name", name.concat("%"));
		}
		
		if(null != phone && !phone.isEmpty()) {
			params.put("name", phone.concat("%"));
		}

		if(priceFrom > 0) {
			params.put("from", priceFrom);
		}

		if(priceTo > 0) {
			params.put("to", priceTo);
		}
		
		return params;
	}
}
