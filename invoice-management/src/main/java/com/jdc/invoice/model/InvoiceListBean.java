package com.jdc.invoice.model;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.invoice.entity.Invoice;
import com.jdc.invoice.service.InvoiceSearch;
import com.jdc.invoice.service.InvoiceService;

@Named
@ViewScoped
public class InvoiceListBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private List<Invoice> list;
	
	@Inject
	private InvoiceSearch search;
	
	@Inject
	private InvoiceService service;
	
	public void search() {
		list = service.search(search);
	}

	public List<Invoice> getList() {
		return list;
	}

	public void setList(List<Invoice> list) {
		this.list = list;
	}

	public InvoiceSearch getSearch() {
		return search;
	}

	public void setSearch(InvoiceSearch search) {
		this.search = search;
	}
	
	
}
