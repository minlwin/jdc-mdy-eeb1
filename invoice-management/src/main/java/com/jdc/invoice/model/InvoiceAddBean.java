package com.jdc.invoice.model;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.jdc.invoice.entity.Invoice;
import com.jdc.invoice.entity.Product;
import com.jdc.invoice.service.InvoiceService;

@Named
@ViewScoped
public class InvoiceAddBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Invoice invoice;
	
	@Inject
	private InvoiceService service;
	
	@PostConstruct
	private void postConstruct() {
		clear();
	}
	
	public void clear() {
		invoice = new Invoice();
		add();
	}
	
	public void add() {
		invoice.addProduct(new Product());
	}
	
	public void remove(Product p) {
		invoice.remove(p);
		if(invoice.getProducts().size() == 0) {
			add();
		}
	}
	
	public String save() {
		service.save(invoice);
		return "/index?faces-redirect=true";
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
}
