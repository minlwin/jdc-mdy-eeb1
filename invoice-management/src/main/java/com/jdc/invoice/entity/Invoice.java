package com.jdc.invoice.entity;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

@Entity
public class Invoice implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private int id;
	private String invoiceNumber;
	private LocalDateTime invoiceDate;
	private String customer;
	private String phone;
	
	@OneToMany(mappedBy = "invoice", cascade = { PERSIST, MERGE }, orphanRemoval = true, fetch = EAGER)
	private List<Product> products;
	
	public Invoice() {
		products = new ArrayList<>();
	}
	
	@PrePersist
	public void prePersit() {
		invoiceDate = LocalDateTime.now();
	}
	
	public void addProduct(Product p) {
		p.setInvoice(this);
		products.add(p);
	}
	
	public void remove(Product p) {
		p.setInvoice(null);
		products.remove(p);
	}
	
	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public int getSubTotal() {
		return products.stream().mapToInt(a -> a.getTotal()).sum();
	}
	
	public double getTax() {
		return getSubTotal() * 0.05;
	}
	
	public double getTotal() {
		return getSubTotal() + getTax();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public LocalDateTime getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(LocalDateTime invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
